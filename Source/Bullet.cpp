#include "../Header/Bullet.h"


Bullet::Bullet(float posX, float posY, float dirX, float dirY, float moveSpeed) {
    this->direction.x = dirX;
    this->direction.y = dirY;
    this->moveSpeed = moveSpeed;

    this->initTexture();
    this->initSprite(posX, posY);
}

Bullet::~Bullet() {

}

void Bullet::initTexture() {
    this->texture.loadFromFile("Assets/bullet.png");
}

void Bullet::initSprite(float posX, float posY) {
    this->sprite.setTexture(this->texture);

    sf::FloatRect spriteRect = this->sprite.getLocalBounds();
    this->sprite.setOrigin(spriteRect.width / 2, spriteRect.height / 2);

    this->sprite.scale(1.f, 1.f);
    this->sprite.setPosition(posX, posY);
}

const sf::FloatRect Bullet::getBounds() const {
    return this->sprite.getGlobalBounds();
}

void Bullet::update() {
    this->sprite.move(this->moveSpeed * this->direction);
}

void Bullet::render(sf::RenderTarget& target) {
    target.draw(this->sprite);
}