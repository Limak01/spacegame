#include "../Header/Player.h"


Player::Player() {
    this->initTexture();
    this->initSprite();
    this->initSounds();
}

Player::~Player() {

}

void Player::initTexture() {
    this->texture.loadFromFile("./Assets/spaceship.png");
}

void Player::initSprite() {
    this->sprite.setTexture(this->texture);

    sf::FloatRect spriteRect = this->sprite.getLocalBounds();
    this->sprite.setOrigin(spriteRect.width / 2, spriteRect.height / 2);

    this->sprite.scale(2.3f, 2.3f);
}

void Player::initSounds() {
    this->shotBuffer.loadFromFile("Assets/shot.wav");
    this->shotSound.setBuffer(this->shotBuffer);
    this->shotSound.setVolume(10.f);
}

void Player::move(const float offsetX, const float offsetY) {
    this->sprite.move(this->movementSpeed * offsetX, this->movementSpeed * offsetY);
}

void Player::shot() {
    this->shotSound.play();
    if(clock.getElapsedTime().asSeconds() > 0.01f) {
        this->bullets.push_back(new Bullet(this->sprite.getPosition().x, this->sprite.getPosition().y, 0.f, -1.f, 10.f));
        //std::cout << "SHOT" << "\n";
    }
    clock.restart();//std::cout << "Player position: " << this->sprite.getPosition().x << " " << this->sprite.getPosition().y << std::endl;
}

void Player::renderBullets(sf::RenderTarget& target) {
    for(auto* bullet: this->bullets) {
        bullet->render(target);
    }
}

void Player::updateBullets() {

    unsigned counter = 0;
    for(auto* bullet: this->bullets) {
        bullet->update();

        if(bullet->getBounds().top + bullet->getBounds().height < 0.f) {
            delete this->bullets.at(counter);

            this->bullets.erase(this->bullets.begin() + counter);
            --counter;
        }

        counter++;
    }
}

void Player::setBullets(std::vector<Bullet*> bullets) {
    this->bullets = bullets;
}

float Player::getXPosition() {
    return this->sprite.getPosition().x;
}

float Player::getYPosition() {
    return this->sprite.getPosition().y;
}

void Player::setPosition(float posX, float posY) {
    this->sprite.setPosition(posX, posY);
}

const sf::FloatRect Player::getBounds() const {
    return this->sprite.getGlobalBounds();
}

std::vector<Bullet*> Player::getBullets() {
    return this->bullets;
}

void Player::update() {
    this->updateBullets();
}

void Player::render(sf::RenderTarget& target) {
    this->renderBullets(target);
    target.draw(this->sprite);
}