#include "../Header/Score.h"

Score::Score() {
    this->score = 0;
    this->font.loadFromFile("Assets/Arial.TTF");
    this->text.setFont(font);
    this->text.setString("Score: " + std::to_string(this->score));
    this->text.setPosition(10.f, 10.f);
}

Score::~Score() {

}

void Score::setScore(int score) {
    this->score = score;
}

int Score::getScore() {
    return this->score;
}

void Score::update() {
    this->text.setString("Score: " + std::to_string(this->score));
}

void Score::render(sf::RenderTarget& target) {
    target.draw(this->text);
}