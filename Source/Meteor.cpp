#include "../Header/Meteor.h"

Meteor::Meteor(float posX, float posY, float dirX, float dirY, float moveSpeed) {
    this->direction.x = dirX;
    this->direction.y = dirY;
    this->moveSpeed = moveSpeed;
    this->initSprite();

    this->sprite.setPosition(posX, posY);
}

Meteor::~Meteor() {
    
}

void Meteor::initSprite() {
    this->texture.loadFromFile("Assets/meteor.png");
    this->sprite.setTexture(texture);

    sf::FloatRect rect = this->sprite.getLocalBounds();
    this->sprite.setOrigin(rect.width / 2, rect.height / 2);

    this->sprite.scale(1.5f, 1.5f);
}

const sf::FloatRect Meteor::getBounds() const {
    return this->sprite.getGlobalBounds();
}

void Meteor::update() {
    this->sprite.move(this->moveSpeed * this->direction);
    this->sprite.rotate(2.f);
}

void Meteor::render(sf::RenderTarget& target) {
    target.draw(this->sprite);
}