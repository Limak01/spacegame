#include "../Header/Window.h"
#include <iostream>

Window::Window() {
    this->meteorsSpeed = 2.5f;
    this->meteorsFrequency = 1.f;
    this->endgame = false;
    this->initWindow();
    this->initWindowSize();
    this->initPlayer();
    this->initSounds();
}

Window::~Window() {
    delete this->window;
    delete this->player;
}

void Window::initWindow() {
    window = new sf::RenderWindow(sf::VideoMode(1280, 720), "Limaks Game", sf::Style::Fullscreen);
    this->window->setFramerateLimit(144);
    this->window->setVerticalSyncEnabled(false);
}

void Window::initWindowSize() {
    this->windowSize = this->window->getSize();
}

void Window::initPlayer() {
    this->player = new Player();
    this->player->setPosition(this->windowSize.x / 2, this->windowSize.y - 40.f);
}

void Window::initSounds() {
    //Background music
    this->bgmusic.openFromFile("Assets/bgmusic.wav");
    this->bgmusic.setVolume(15.f);

    //Endgame sound
    this->endgameBuffer.loadFromFile("Assets/lostgame.wav");
    this->endgameSound.setBuffer(this->endgameBuffer);
    this->endgameSound.setVolume(10.f);

    //Destryo meteor sound
    this->destroyMeteorBuffer.loadFromFile("Assets/destroy.wav");
    this->destroyMeteorSound.setBuffer(this->destroyMeteorBuffer);
    this->destroyMeteorSound.setVolume(10.f);
}

void Window::run() {
    bgmusic.play();
    while(this->window->isOpen()) {
        this->update();
        this->render();
    }
}

void Window::renderMeteors() {
    for(Meteor* meteor: meteors) {
        meteor->render(*this->window);
    }
}

void Window::updateMeteors() {
    std::vector<Bullet*> bullets = this->player->getBullets();
    unsigned counter = 0;
    for(Meteor* meteor: meteors) {
        meteor->update();

        unsigned bulletCounter = 0;
        for(Bullet* bullet: bullets) {
            //If bullet hits meteor
            if(bullet->getBounds().intersects(meteor->getBounds())) {
                this->destroyMeteorSound.play();
                delete meteors.at(counter);

                delete bullets.at(bulletCounter);
                bullets.erase(bullets.begin() + bulletCounter);

                this->player->setBullets(bullets);

                meteors.erase(meteors.begin() + counter);

                this->score.setScore(this->score.getScore() + 10);

                --counter;
                --bulletCounter;
            }

            bulletCounter ++;
        }

        if(meteor->getBounds().intersects(this->player->getBounds())) {
            this->endgameSound.play();
            this->score.setScore(0);
            this->meteors = {};
            this->meteorsSpeed = 2.5f;
            this->meteorsFrequency = 1.f;
            this->bgmusic.stop();
            this->endgame = true;
        }

        if(meteor->getBounds().top > this->windowSize.y) {
            delete meteors.at(counter);

            this->score.setScore(this->score.getScore() - 5);

            meteors.erase(meteors.begin() + counter);
            --counter;
        }

        counter ++;
    }
}

void Window::update() {
    sf::Event event;
    srand(time(NULL));
    while(this->window->pollEvent(event)) {
        if(event.type == sf::Event::Closed) {
            this->window->close();
        }

        if(event.KeyPressed && event.key.code == sf::Keyboard::Escape) {
            this->window->close();
        }

    }

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::D) && this->player->getXPosition() < this->windowSize.x)
        this->player->move(1.f, 0);

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::A) && this->player->getXPosition() > 0) 
        this->player->move(-1.f, 0);

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::W) && this->player->getYPosition() > 0) 
        this->player->move(0, -1.f);

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::S) && this->player->getYPosition() < this->windowSize.y) 
        this->player->move(0, 1.f);

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Enter))
        this->player->shot();

    if(clock.getElapsedTime().asSeconds() > this->meteorsFrequency) {
        this->meteors.push_back(new Meteor(rand() % this->windowSize.x, 0.f, 0.f, 1.f, this->meteorsSpeed));
        clock.restart();
    }

    if(this->endgame && sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
        this->player->setPosition(this->windowSize.x / 2, this->windowSize.y - 40.f);
        this->bgmusic.play();
        this->endgame = false;
    }

    if(this->score.getScore() >= 500) {
        this->meteorsSpeed = 3.f;
        this->meteorsFrequency = .5f;
    }

    this->player->update();
    this->updateMeteors();
    this->score.update();
    
}

void Window::render() {
    this->window->clear();
    if(!endgame) {
        this->player->render(*this->window);
        this->renderMeteors();
        this->score.render(*this->window);
    } else {
        sf::Font font;
        sf::Text text;

        font.loadFromFile("Assets/Arial.TTF");

        text.setFont(font);
        text.setString("You Lost\n To play again click space");
        text.setPosition(this->windowSize.x / 2, this->windowSize.y / 2);
        text.setOrigin(text.getLocalBounds().width / 2, text.getLocalBounds().height / 2);
        text.setCharacterSize(40);
        this->window->draw(text);
    }

    this->window->display();
}