#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Audio.hpp>
#include "Bullet.h"
#include <vector>
#include <iostream>

class Player {
    private:
        sf::Texture texture;
        sf::Sprite sprite;
        sf::Clock clock;
        sf::SoundBuffer shotBuffer;
        sf::Sound shotSound;
        std::vector<Bullet*> bullets;
        float movementSpeed = 5;

        void initTexture();
        void initSprite();
        void initSounds();
    public:
        Player();
        ~Player();

        const sf::FloatRect getBounds() const;
        void move(const float offsetX, const float offsetY);
        void shot();
        void setPosition(float posX, float posY);
        void renderBullets(sf::RenderTarget& target);
        void updateBullets();
        void setBullets(std::vector<Bullet*> bullets);
        float getXPosition();
        float getYPosition();
        std::vector<Bullet*> getBullets();
        void update();
        void render(sf::RenderTarget& target);
};