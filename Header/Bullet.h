#pragma once

#include <SFML/Graphics.hpp>

class Bullet {
    private:
        sf::Texture texture;
        sf::Sprite sprite;
        sf::Vector2f direction;
        float moveSpeed;

        void initTexture();
        void initSprite(float posX, float posY);

    public:
        Bullet(float posX, float posY, float dirX, float dirY, float moveSpeed);
        ~Bullet();

        const sf::FloatRect getBounds() const;
        void update();
        void render(sf::RenderTarget& traget);
};