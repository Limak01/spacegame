#pragma once

#include <SFML/Graphics.hpp>
#include <iostream>

class Score {
    private:
        sf::Font font;
        sf::Text text;
        int score;

        void initScore();

    public:
        Score();
        ~Score();

        void setScore(int score);
        int getScore();
        void update();
        void render(sf::RenderTarget& target);
};