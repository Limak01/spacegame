#pragma once

#include <SFML/Audio.hpp>
#include "Player.h"
#include "Meteor.h"
#include "Bullet.h"
#include "Score.h"
#include <stdlib.h>
#include <vector>

class Window {
    private:
        sf::RenderWindow* window;
        Player* player;
        sf::Vector2u windowSize;
        std::vector<Meteor*> meteors;
        sf::Music bgmusic;
        sf::SoundBuffer endgameBuffer;
        sf::SoundBuffer destroyMeteorBuffer;
        sf::Sound endgameSound;
        sf::Sound destroyMeteorSound;
        sf::Clock clock;
        Score score;
        float meteorsSpeed;
        float meteorsFrequency;
        bool endgame;

        void initWindow();
        void initPlayer();
        void initWindowSize();
        void initSounds();

    public:
        Window();
        ~Window();

        void run();
        void renderMeteors();
        void updateMeteors();
        void update();
        void render();
};