#pragma once

#include <SFML/Graphics.hpp>

class Meteor {
    private:
        sf::Texture texture;
        sf::Sprite sprite;
        sf::Vector2f direction;
        float moveSpeed;

        void initSprite();

    public:
        Meteor(float posX, float posY, float dirX, float dirY, float moveSpeed);
        ~Meteor();

        const sf::FloatRect getBounds() const;
        void update();
        void render(sf::RenderTarget& target);
};